from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from presentations.views import PresentationView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^p/(?P<slug>[-\w]+)/$',PresentationView.as_view(),name="presentation"),
    url(r'^admin/', include(admin.site.urls)),
)
