# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Changing field 'Presentation.creation_time'
        db.alter_column('presentations_presentation', 'creation_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Slide.creation_time'
        db.alter_column('presentations_slide', 'creation_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Adding unique constraint on 'Slide', fields ['presentation', 'order']
        db.create_unique('presentations_slide', ['presentation_id', 'order'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Slide', fields ['presentation', 'order']
        db.delete_unique('presentations_slide', ['presentation_id', 'order'])

        # Changing field 'Presentation.creation_time'
        db.alter_column('presentations_presentation', 'creation_time', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Slide.creation_time'
        db.alter_column('presentations_slide', 'creation_time', self.gf('django.db.models.fields.DateTimeField')())


    models = {
        'presentations.presentation': {
            'Meta': {'ordering': "('modification_time', 'name')", 'object_name': 'Presentation'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'presentations.slide': {
            'Meta': {'unique_together': "(('order', 'presentation'),)", 'object_name': 'Slide'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_x': ('django.db.models.fields.IntegerField', [], {}),
            'data_y': ('django.db.models.fields.IntegerField', [], {}),
            'data_z': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'presentation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['presentations.Presentation']"}),
            'rotate_x': ('django.db.models.fields.IntegerField', [], {}),
            'rotate_y': ('django.db.models.fields.IntegerField', [], {}),
            'rotate_z': ('django.db.models.fields.IntegerField', [], {}),
            'scale': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'title_slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'blank': 'True', 'populate_from': 'None', 'unique': 'True', 'null': 'True', 'db_index': 'True'})
        }
    }

    complete_apps = ['presentations']
