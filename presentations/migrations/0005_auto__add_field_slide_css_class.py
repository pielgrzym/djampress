# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Slide.css_class'
        db.add_column('presentations_slide', 'css_class', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Slide.css_class'
        db.delete_column('presentations_slide', 'css_class')


    models = {
        'presentations.presentation': {
            'Meta': {'ordering': "('modification_time', 'name')", 'object_name': 'Presentation'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': 'None', 'db_index': 'True'})
        },
        'presentations.slide': {
            'Meta': {'unique_together': "(('order', 'presentation'),)", 'object_name': 'Slide'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'css_class': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'data_x': ('django.db.models.fields.IntegerField', [], {}),
            'data_y': ('django.db.models.fields.IntegerField', [], {}),
            'data_z': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'presentation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['presentations.Presentation']"}),
            'rotate_x': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rotate_y': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rotate_z': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'scale': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'title_slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'blank': 'True', 'populate_from': 'None', 'unique': 'True', 'null': 'True', 'db_index': 'True'})
        }
    }

    complete_apps = ['presentations']
