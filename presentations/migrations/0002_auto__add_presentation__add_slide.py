# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Presentation'
        db.create_table('presentations_presentation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creation_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('modification_time', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('presentations', ['Presentation'])

        # Adding model 'Slide'
        db.create_table('presentations_slide', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creation_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('presentation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['presentations.Presentation'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=250, unique=True, null=True, blank=True)),
            ('title_slug', self.gf('autoslug.fields.AutoSlugField')(unique_with=(), max_length=50, blank=True, populate_from=None, unique=True, null=True, db_index=True)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('modification_time', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('data_x', self.gf('django.db.models.fields.IntegerField')()),
            ('data_y', self.gf('django.db.models.fields.IntegerField')()),
            ('data_z', self.gf('django.db.models.fields.IntegerField')()),
            ('scale', self.gf('django.db.models.fields.IntegerField')()),
            ('rotate_x', self.gf('django.db.models.fields.IntegerField')()),
            ('rotate_y', self.gf('django.db.models.fields.IntegerField')()),
            ('rotate_z', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('presentations', ['Slide'])


    def backwards(self, orm):
        
        # Deleting model 'Presentation'
        db.delete_table('presentations_presentation')

        # Deleting model 'Slide'
        db.delete_table('presentations_slide')


    models = {
        'presentations.presentation': {
            'Meta': {'ordering': "('modification_time', 'name')", 'object_name': 'Presentation'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'presentations.slide': {
            'Meta': {'object_name': 'Slide'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'creation_time': ('django.db.models.fields.DateTimeField', [], {}),
            'data_x': ('django.db.models.fields.IntegerField', [], {}),
            'data_y': ('django.db.models.fields.IntegerField', [], {}),
            'data_z': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modification_time': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'presentation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['presentations.Presentation']"}),
            'rotate_x': ('django.db.models.fields.IntegerField', [], {}),
            'rotate_y': ('django.db.models.fields.IntegerField', [], {}),
            'rotate_z': ('django.db.models.fields.IntegerField', [], {}),
            'scale': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'title_slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'blank': 'True', 'populate_from': 'None', 'unique': 'True', 'null': 'True', 'db_index': 'True'})
        }
    }

    complete_apps = ['presentations']
