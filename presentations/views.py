# Create your views here.
from django.views.generic.detail import DetailView
from presentations.models import Presentation

class PresentationView(DetailView):
    model = Presentation
    template_name = 'presentations/presentation.html'
    context_object_name = 'presentation'

