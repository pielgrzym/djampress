# -*- coding: utf-8 -*-
from django import forms
from presentations.models import Slide

class SlideForm(forms.ModelForm):
    class Meta:
        model = Slide

    def clean_title(self):
        return self.cleaned_data['title'] or None

    def clean_title_slug(self):
        return self.cleaned_data['title_slug'] or None

