# -*- coding: utf-8 -*-
from django.contrib import admin
from presentations.forms import SlideForm
from presentations.models import Presentation, Slide

class PresentationAdmin(admin.ModelAdmin):
    pass

admin.site.register(Presentation,PresentationAdmin)

class SlideAdmin(admin.ModelAdmin):
    form = SlideForm
    ordering = ('-order',)

    list_display = ('__unicode__','order','data_x','data_y','data_z','rotate_x','rotate_y','rotate_z','scale')
    list_editable = ('order','data_x','data_y','data_z','rotate_x','rotate_y','rotate_z','scale')

admin.site.register(Slide,SlideAdmin)
