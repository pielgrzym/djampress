from autoslug import AutoSlugField
from django.db import models

from django.utils.translation import ugettext as _

# Create your models here.

class Presentation(models.Model):
    name = models.CharField(max_length=250,verbose_name=_("Name"))
    slug = AutoSlugField(populate_from='name')
    creation_time = models.DateTimeField(auto_now_add=True,editable=False,verbose_name=_("Creation time"))
    modification_time = models.DateTimeField(auto_now=True,editable=False,verbose_name=_("Modification time"))

    class Meta:
        verbose_name = _("Presentation")
        verbose_name_plural = _("Presentations")
        ordering = ('modification_time','name')

    def __unicode__(self):
        return self.name

class Slide(models.Model):
    presentation = models.ForeignKey(Presentation,verbose_name=_("Presentation"))
    title = models.CharField(max_length=250,null=True,blank=True,unique=True,verbose_name=_("Title"))
    title_slug = AutoSlugField(populate_from='title',unique=True,verbose_name=_("Slug"),null=True,blank=True)
    css_class = models.CharField(max_length=250, null=True,blank=True)
    content = models.TextField(verbose_name=_("Content"))
    creation_time = models.DateTimeField(auto_now_add=True,editable=False,verbose_name=_("Creation time"))
    modification_time = models.DateTimeField(auto_now=True,editable=False,verbose_name=_("Modification time"))

    order = models.PositiveIntegerField(verbose_name=_("Ordering"))

    data_x = models.IntegerField(verbose_name=_("X coordinate"),help_text=_("X coordinate of the center of slide"), default=0)
    data_y = models.IntegerField(verbose_name=_("Y coordinate"),help_text=_("Y coordinate of the center of slide"), default=0)
    data_z = models.IntegerField(verbose_name=_("Z coordinate"),help_text=_("Z coordinate of the center of slide"), default=0)

    scale = models.IntegerField(verbose_name=_("Scale"),default=1)

    rotate_x = models.IntegerField(verbose_name=_("Rotation over X axis"),default=0)
    rotate_y = models.IntegerField(verbose_name=_("Rotation over Y axis"),default=0)
    rotate_z = models.IntegerField(verbose_name=_("Rotation over Z axis"),default=0)

    class Meta:
        verbose_name = _("Slide")
        verbose_name_plural = _("Slides")
        unique_together = ('order','presentation')
        ordering = ('order',)



    def save(self, **kwargs):
        self.presentation.save()
        return super(Slide,self).save(**kwargs)

    def __unicode__(self):
        return self.title or self.presentation


